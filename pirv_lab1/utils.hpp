//
//  utils.hpp
//  pirv_lab1
//
//  Created by Anton Dosov on 02.10.16.
//  Copyright © 2016 Anton Dosov. All rights reserved.
//

#ifndef utils_hpp
#define utils_hpp

#include <stdio.h>
#include <ctime>
#include <iostream>
#include <stdlib.h>
// #include <malloc.h>
#include <mach/mach.h>
#include <mach/mach_time.h>

using namespace std;

double getTime(double first, double second);


double realtime(void);
void matrix_init(double **m, int n , int k);
void matrix_init_number(double **m, int n , int k, double number);
void print_matrix(double **m, int n , int k);
double **alloc_2d_array(int rows, int cols);

#endif /* utils_hpp */
