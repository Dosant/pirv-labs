//
//  utils.cpp
//  pirv_lab1
//
//  Created by Anton Dosov on 02.10.16.
//  Copyright © 2016 Anton Dosov. All rights reserved.
//

#include <stdlib.h>
#include "utils.hpp"

using namespace std;


double realtime() /* returns time in seconds */
{
  static double timeConvert = 0.0;
  if ( timeConvert == 0.0 )
  {
    mach_timebase_info_data_t timeBase;
    (void)mach_timebase_info( &timeBase );
    timeConvert = (double)timeBase.numer /
    (double)timeBase.denom /
    1000000000.0;
  }
  return (double)mach_absolute_time( ) * timeConvert;
}

  
double getTime(double tstart, double tend) {
    cout << (double) CLOCKS_PER_SEC << endl;
    cout << tend << endl;
    cout << tstart << endl;
    return (tend - tstart) / (double) CLOCKS_PER_SEC;
}

void matrix_init(double **m, int n, int k) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < k; j++) {
            m[i][j] = (rand() % 200) - 100;
        }
    }
}

void matrix_init_number(double **m, int n, int k, double number) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < k; j++) {
            m[i][j] = number;
        }
    }
}

void print_matrix(double **m, int n, int k) {
    printf("******************************************************\n");
    printf("Result Matrix:\n");
    for (int i = 0; i < n; i++) {
        printf("\n");
        for (int j = 0; j < k; j++) {
            printf("%6.2f   ", m[i][j]);
        }
    }
    printf("\n******************************************************\n");
}

double **alloc_2d_array(int rows, int cols) {
    int i;
    double *data = (double *) malloc(rows * cols * sizeof(double));
    double **array = (double **) malloc(rows * sizeof(double *));
    for (i = 0; i < rows; i++)
        array[i] = &(data[cols * i]);

    return array;
}