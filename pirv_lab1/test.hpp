//
//  test.hpp
//  pirv_lab1
//
//  Created by Anton Dosov on 02.10.16.
//  Copyright © 2016 Anton Dosov. All rights reserved.
//

#ifndef test_hpp
#define test_hpp

#include <stdio.h>


bool test(double **C1, double **C2, int n, int m);

#endif /* test_hpp */
