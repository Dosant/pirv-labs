//
//  main.cpp
//  pirv_lab1
//

#include <iostream>
#include <stdio.h>
#include "utils.hpp"
#include "test.hpp"

using namespace std;

struct process_output {
	double **result;
	double duration;
};

void multiply_matrices(double **A, double **B, double **C, int N1, int N2, int N3, bool isParallel, int nThreads);

void multiply_matrices_block_local(double **A, double **B, double **C, int N1, int N2, int N3, int R, bool isParallel,
								   int nThreads);

void multiply_matrices_block_global(double **A, double **B, double **C, int N1, int N2, int N3, int R, bool isParallel,
									int nThreads);

process_output process(double **A, double **B, int N, int R, bool isGlobalParallel, bool isLocalParallel, int nThreads);


int main(int argc, const char *argv[]) {
	process_output result;
	
	double **A, **B;
	
	// sequential
	int N[] = {500, 1000, 2000};
	int R[] = {1, 10, 20, 50, 100, 250};
	for (int i = 0; i < 3; i++) {
		cout << N[i] << " ";
	}
	cout << endl;
	
	for (int i = 0; i < 3; i++) {
		int curN = N[i];
		A = alloc_2d_array(curN, curN);
		B = alloc_2d_array(curN, curN);
		matrix_init(A, curN, curN);
		matrix_init(B, curN, curN);
		
		for (int j = 0; j < 6; j++) {
			int curR = R[j];
			result = process(A, B, curN, curR, false, false, 1);
			cout << curR << " " << result.duration << " ";
		}
		
		cout << endl;
		
		free(A);
		free(B);
		free(result.result);
	}
	cout << endl;
	for (int i = 0; i < 6; i++) {
		cout << R[i] << " ";
	}
	cout << endl;
	
	int N_Threads[] = {1, 2, 4, 8};
	
	// global parallel
	for (int i = 0; i < 3; i++) {
		int curN = N[i];
		A = alloc_2d_array(curN, curN);
		B = alloc_2d_array(curN, curN);
		matrix_init(A, curN, curN);
		matrix_init(B, curN, curN);
		
		for (int j = 0; j < 6; j++) {
			int curR = R[j];
			
			for (int k = 0; k < 4; k++) {
				int cur_N_Threads = N_Threads[k];
				result = process(A, B, curN, curR, true, false, cur_N_Threads);
				cout << cur_N_Threads << " " << result.duration << " ";
			}
			
			cout << endl;
		}
		cout << endl;
		
		free(A);
		free(B);
		free(result.result);
	}
	
	// local parrallel
	for (int i = 0; i < 3; i++) {
		int curN = N[i];
		A = alloc_2d_array(curN, curN);
		B = alloc_2d_array(curN, curN);
		matrix_init(A, curN, curN);
		matrix_init(B, curN, curN);
		
		for (int j = 0; j < 6; j++) {
			int curR = R[j];
			
			for (int k = 0; k < 4; k ++) {
				int cur_N_Threads = N_Threads[k];
				result = process(A, B, curN, curR, false, true, cur_N_Threads);
				cout << cur_N_Threads << " " << result.duration << " ";
			}
			
			cout << endl;
		}
		cout << endl;
		
		free(A);
		free(B);
		free(result.result);
	}
	
	// amomally case
	
	//    for (int i = 0; i < 3; i++) {
	//        int curN = N[i];
	//        A = alloc_2d_array(curN, curN);
	//        B = alloc_2d_array(curN, curN);
	//        matrix_init(A, curN, curN);
	//        matrix_init(B, curN, curN);
	//
	//        for (int k = 0; k < 4; k++) {
	//            int cur_N_Threads = N_Threads[k];
	//            result = process(A, B, curN, 1, true, false, cur_N_Threads);
	//            cout << cur_N_Threads << " " << result.duration << " ";
	//        }
	//
	//        cout << endl;
	//    }
	
	
	
	return 0;
}

process_output
process(double **A, double **B, int N, int R, bool isGlobalParallel, bool isLocalParallel, int nThreads) {
	double **C;
	double time_s, time_e;
	
	C = alloc_2d_array(N, N);
	matrix_init_number(C, N, N, 0.0);
	
	// time_s = clock();
	time_s = realtime();
	// Non block
	if (R == 1) {
		multiply_matrices(A, B, C, N, N, N, isGlobalParallel, nThreads);
	} else {
		if (isGlobalParallel) {
			multiply_matrices_block_global(A, B, C, N, N, N, R, true, nThreads);
		} else if (isLocalParallel) {
			multiply_matrices_block_local(A, B, C, N, N, N, R, true, nThreads);
		} else {
			multiply_matrices_block_global(A, B, C, N, N, N, R, false, nThreads);
		}
	}
	
	time_e = realtime();
	// time_e = clock();
	
	process_output result;
	
	//result.duration = getTime(time_s, time_e);
	result.duration = time_e - time_s;
	result.result = C;
	
	return result;
}


void multiply_matrices(double **A, double **B, double **C, int N1, int N2, int N3, bool isParallel, int nThreads) {
	
#pragma omp parallel for if(isParallel) num_threads(nThreads)
	for (int i = 0; i < N1; i++) {
		for (int j = 0; j < N3; j++) {
			for (int k = 0; k < N2; k++) {
				C[i][j] = C[i][j] + A[i][k] * B[k][j];
			}
		}
	}
}

void multiply_matrices_block_global(double **A, double **B, double **C, int N1, int N2, int N3, int R, bool isParallel,
									int nThreads) {
	
	const int Q1 = N1 / R;
	const int Q2 = N3 / R;
	const int Q3 = N2 / R;
	
#pragma omp parallel for if(isParallel) num_threads(nThreads)
	for (int i_gl = 0; i_gl < Q1; i_gl++) {
		for (int j_gl = 0; j_gl < Q2; j_gl++) {
			for (int k_gl = 0; k_gl < Q3; k_gl++) {
				
				// local block
				for (int i = i_gl * R; i < (i_gl + 1) * R; i++) {
					for (int j = j_gl * R; j < (j_gl + 1) * R; j++) {
						for (int k = k_gl * R; k < (k_gl + 1) * R; k++) {
							C[i][j] = C[i][j] + A[i][k] * B[k][j];
						}
					}
				}
				
			}
		}
	}
}

void multiply_matrices_block_local(double **A, double **B, double **C, int N1, int N2, int N3, int R, bool isParallel,
								   int nThreads) {
	
	const int Q1 = N1 / R;
	const int Q2 = N3 / R;
	const int Q3 = N2 / R;
	
	for (int i_gl = 0; i_gl < Q1; i_gl++) {
#pragma omp parallel for if(isParallel) num_threads(nThreads)
		for (int j_gl = 0; j_gl < Q2; j_gl++) {
			for (int k_gl = 0; k_gl < Q3; k_gl++) {
				for (int i = i_gl * R; i < (i_gl + 1) * R; i++) {
					for (int j = j_gl * R; j < (j_gl + 1) * R; j++) {
						for (int k = k_gl * R; k < (k_gl + 1) * R; k++) {
							C[i][j] = C[i][j] + A[i][k] * B[k][j];
						}
					}
				}
				
			}
		}
	}
}





