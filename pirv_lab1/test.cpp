//
//  test.cpp
//  pirv_lab1
//
//  Created by Anton Dosov on 02.10.16.
//  Copyright © 2016 Anton Dosov. All rights reserved.
//

#include <iostream>
#include "test.hpp"

using namespace std;

bool test(double **C1, double **C2, int n, int m) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (C1[i][j] != C2[i][j]) {
                return false;
            }
        }
    }

    return true;
}
